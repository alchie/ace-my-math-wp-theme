</div><!-- #wrap -->
<div id="footer">
    <?php get_sidebar('frontpage-footer'); ?>

    <div id="bottom-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="copyright"> Copyrigth &copy; <?php echo date('Y'); ?> - Ace My Math - All rights reserved</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
