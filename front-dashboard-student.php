 <div id="main-container" class="lessons-list">
    <div class="container">
      <div class="row">

<div class="col-md-12">

<div class="table-responsive whitebox add-padding">
   <table class="table table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Lesson Title</th>
            <th>Last Taken</th>
            <th>Rating</th>
            <th>Score</th>
            <th>Status</th>    
            <th></th> 
            <th></th>            
          </tr>
        </thead>
        <tbody>

<?php 
global $current_user;
get_currentuserinfo();
$result = aceLessonProgressByUser($current_user->ID);

foreach($result as $progress) :
?>
        <tr>
            <td></td>
            <td><a href="<?php echo get_permalink( $progress->lesson_id ); ?>"><?php echo get_the_title( $progress->lesson_id); ?></a></td>
            <td><?php echo $progress->date_taken ?></td>
            <td></td>
            <td></td>            
            <td></td>  
            <td><a href=""><span class="glyphicon glyphicon-print"></span></a></td>
            <td><a href=""><span class="glyphicon glyphicon-list"></span></a></td>            
          </tr>
<?php endforeach; ?>

        </tbody>
   </table>
</div>


</div>

      </div>
    </div>
 </div>

