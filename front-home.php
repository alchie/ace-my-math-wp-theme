<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrap">
    
<div id="header-container">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            
           <?php get_template_part('header', 'right'); ?>
            
              <div id="branding">
                <h1 id="site-title"><a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                 <?php bloginfo( 'name' ); ?></a></h1>
                 <h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
                
             </div>

<div class="shadowed rounded">
 
    <!-- #site-navigation -->
<div id="site-navigation">
 
<?php
$item_wrap = '
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#">Navigation</a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1"><ul id="%1$s" class="%2$s">%3$s</ul>'.get_search_form(false).'</div>';

 wp_nav_menu( array(
    'theme_location'    => 'header-primary',
    'container'     => 'nav',
    'container_id'      => 'header-primary',
    'container_class'   => 'navbar navbar-default',
    'menu_class'        => 'nav navbar-nav navbar-left', 
    'echo'          => true,
    'items_wrap'        => $item_wrap,
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>
</div>
     <div id="carousel-slider-container" class="carousel slide" data-ride="carousel">
        
        <div class="carousel-inner">
          <div class="item active">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider.jpg" alt="First slide">
          </div>
          <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider.jpg" alt="First slide">
          </div>
          <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider.jpg" alt="First slide">
          </div>
        </div>
        <div class="controls">
            <div class="left">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-slider-container" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-slider-container" data-slide-to="1"></li>
                  <li data-target="#carousel-slider-container" data-slide-to="2"></li>
                </ol>
            </div>
            <div class="right">
                <a class="left carousel-control" href="#carousel-slider-container" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-slider-container" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
      </div>
 
</div>

        </div><!-- .col -->  
    </div><!-- .row -->
    </div><!-- .container -->
    </div><!-- #header-container -->
    

<!----------------------------------------------------------------------------------------------- //-->
 <div id="feature-container">
 
 <div class="feature bordered">
    <div class="container">
         <div class="container-inner">
    <div class="row">
    
<?php 

$n = 0;
$levels = array_slice( get_terms('level'), 0, 6); 
if( count($levels) > 0 ) :
    foreach($levels as $level) :
?>
    
        <div class="col-md-4">
        
        <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo $level->name; ?> Lessons</h3>
  </div>
  <div class="panel-body">
    
    <?php
    $lessons = new WP_Query(array(
        'post_type' => 'lesson',
        'posts_per_page' => 5,
        'tax_query' => array(
		    array(
			    'taxonomy' => 'level',
			    'field' => 'term_id',
			    'terms' => $level->term_id
		    )
	    ),
	    'orderby' => 'rand',
    )
    );
    
    if($lessons->have_posts()) :
        while( $lessons->have_posts() ) : $lessons->the_post();
?>
  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>, 
  <?php endwhile; 
  endif;
  wp_reset_query();
  ?>
    
    
  </div>
  <div class="panel-footer"><a href="<?php echo get_term_link( $level, 'level' ); ?>" class="btn btn-success btn-block btn-xs">View More...</a></div>
</div>
              
        </div> 
        
<?php 
$n++;
if( $n == 3 ) {
?>


 </div>
    </div>
    </div>
</div>

<!----------------------------------------------------------------------------------------------- //-->

 <div class="feature bordered">
    <div class="container">
         <div class="container-inner">
    <div class="row">
    
<?php
$n=0;
}
endforeach;
endif;

?>   

          
   
       
    </div>
    </div>
    </div>
</div>

</div>
<!----------------------------------------------------------------------------------------------- //-->

 <div id="video-container" class="bordered">
    <div class="container">
        <div class="container-inner">
            <div class="row">
                <div class="col-md-8">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon1.jpg" class="pull-left">
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non euismod elit, vel interdum turpis. In hac habitasse platea dictumst. Aenean fringilla velit tincidunt gravida vulputate. Praesent condimentum mauris nisi, in tempus enim porttitor vel. Duis at arcu porttitor, hendrerit velit sed, lacinia neque. Praesent et tincidunt ligula. Nunc volutpat nisi ac augue auctor pharetra. Nam elit nisi, posuere quis mi vel, hendrerit dignissim mauris. Etiam et odio eget libero laoreet scelerisque vehicula non lectus. Etiam sit amet ultricies turpis. Sed sodales lacus at turpis volutpat, dignissim tempor tellus viverra. Sed quis nisl eros. Maecenas rhoncus facilisis tortor volutpat faucibus. Nam commodo semper elit, in luctus quam pharetra at. Sed ut ipsum venenatis quam interdum placerat et ac nunc.
                    </p>
                </div>   
                 <div class="col-md-4">
                    <iframe width="100%" height="220" src="//www.youtube.com/embed/5rLgiClm0Uc" frameborder="0" allowfullscreen></iframe>
                </div>  
            </div>
        </div>
    </div>
 </div>
<!----------------------------------------------------------------------------------------------- //-->
