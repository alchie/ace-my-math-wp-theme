<?php if ( is_user_logged_in() ) : ?>
<?php get_header(); 
if( get_user_role() == 'parent' ) {
    get_template_part('front', 'dashboard-parent');
} elseif( get_user_role() == 'student' ) {
    get_template_part('front', 'dashboard-student');
}  ?>
<?php else: ?>
<?php get_template_part('front', 'home'); ?>
<?php endif; // login check ?>
<?php get_footer(); ?>
