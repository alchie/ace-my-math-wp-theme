<?php

function acemymath_theme_setup() {
	

    require('inc/customizer_framework.php');
    require('inc/bootstrap3-menu-walker.php');

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	// This theme uses wp_nav_menu() in one location.
    register_nav_menu( 'header-primary', __( 'Primary Nav', 'acemymath_theme' ) );
    register_nav_menu( 'header-primary-parent', __( 'Parent Nav', 'acemymath_theme' ) );
    register_nav_menu( 'header-primary-student', __( 'Student Nav', 'acemymath_theme' ) );   
    register_nav_menu( 'header-primary-members', __( 'Other Members', 'acemymath_theme' ) );            
	register_nav_menu( 'header-mobile', __( 'Mobile Nav', 'acemymath_theme' ) );
    register_nav_menu( 'footer', __( 'Footer Links', 'acemymath_theme' ) );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	
}
add_action( 'after_setup_theme', 'acemymath_theme_setup' );


/******************************************************************

        Add Scripts and Styles

*******************************************************************/
function acemymath_theme_scripts_styles() {
	// Bootstrap 3
	wp_enqueue_script( 'bootstrap3-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '2014-03-03', true );
    //wp_enqueue_style( 'bootstrap3-css', get_template_directory_uri() . "/css/bootstrap.min.css", array(), null );
    wp_enqueue_style( 'bootstrap3-css', get_template_directory_uri() . "/css/yeti.css", array(), null );
    
    wp_enqueue_script( 'acemymath.custom.js', get_template_directory_uri() . '/js/acemymath.custom.js', array( 'jquery', 'bootstrap3-js', 'fancybox' ), '2014-03-03', true );

    // fancybox
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array( 'jquery' ), '2014-03-03', true );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . "/js/fancybox/jquery.fancybox.css", array(), '2014-03-03' );
    
	// Loads our main stylesheet.
	//wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), '2013-07-18' );
	wp_enqueue_style( 'default-css', get_template_directory_uri() . "/css/default.css", array(), '2014-03-03' );
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . "/css/custom.css", array('default-css'), '2014-03-03' );
	wp_enqueue_style( 'custom-responsive', get_template_directory_uri() . "/css/responsive.css", array('default-css', 'custom-css'), '2014-03-03' );
	
	//Google Fonts
    wp_enqueue_style( 'Sue-Ellen-Francisco', 'http://fonts.googleapis.com/css?family=Sue+Ellen+Francisco', array(), '2014-03-03' );
    
    // JW player
    wp_enqueue_script( 'jwplayer', 'http://jwpsrv.com/library/73yjPrO5EeOQuCIACrqE1A.js', array(), '2014-03-29', false );

}
add_action( 'wp_enqueue_scripts', 'acemymath_theme_scripts_styles' );

function acemymath_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Sidebar', 'acemymath_theme' ),
		'id'            => 'sidebar-main',
		'description'   => __( 'Main Sidebar', 'acemymath_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 1', 'acemymath_theme' ),
		'id'            => 'footer-1',
		'description'   => __( 'Footer 1', 'acemymath_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 2', 'acemymath_theme' ),
		'id'            => 'footer-2',
		'description'   => __( 'Footer 2', 'acemymath_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 3', 'acemymath_theme' ),
		'id'            => 'footer-3',
		'description'   => __( 'Footer 3', 'acemymath_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Frontpage Footer', 'acemymath_theme' ),
		'id'            => 'frontpage-footer',
		'description'   => __( 'Frontpage Footer', 'acemymath_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
}
add_action( 'widgets_init', 'acemymath_theme_widgets_init' );

function get_user_role() {
	global $current_user;

	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);

	return $user_role;
}

function is_user_role($role) {
	global $current_user;

	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);

	if( $user_role == $role ) {
	    return true;
	}
}
