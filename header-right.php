<?php
    $current_user = wp_get_current_user();
    $membership_options = get_option('membership_options');
    
?>

 <ul class="social-icons pull-right">
                <li><a href="#email" class="email">Email</a></li>
                <li><a href="#facebook" class="facebook">Facebook</a></li>
                <li><a href="#twitter" class="twitter">Twitter</a></li>
                <li><a href="#rss" class="rss">RSS</a></li>                                                
 </ul>
 <div class="header-account-form">
  <?php if ( is_user_logged_in() ) {  ?>
  
 <div class="my-account"> Welcome back, <?php echo $current_user->display_name; ?>! <!--&middot; <a href="<?php bloginfo('home'); ?>/account/">My Account</a> &middot; <a href="<?php echo wp_logout_url(); ?> ">Logout</a>--></div>
 
 <?php } else { ?>
        <form name="loginform" id="loginform" class="login-container" action="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>" method="post">
        <div class="login-form">
            <label>User Login</label>
            <?php wp_nonce_field( 'login_ace_account' ); ?>
            <input type="hidden" name="action" value="login_ace_account" />
            <input type="text" name="username" placeholder="Username" value="" >
            <input type="password" name="password" placeholder="Password" value="" >       
            <input type="submit" name="wp-submit" value="Login" class="btn btn-success btn-sm"> 
            <input type="hidden" name="redirect_to" value="<?php bloginfo('home'); ?>" />
		    <input type="hidden" name="testcookie" value="1" />
        </div>
    </form>
    <div class="register">
        <small>Not yet a member?</small>
        <a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>" class="btn btn-danger btn-sm">Signup</a>
    </div>
     <?php } ?>
 </div>

