<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrap">
    
    <div id="header-container">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
           
           <?php get_template_part('header', 'right'); ?>
           
              <div id="branding">
                <h1 id="site-title"><a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                 <?php bloginfo( 'name' ); ?></a></h1>
                 <h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
                
             </div>
            
<!-- #site-navigation -->
<div id="site-navigation">
 <?php if ( is_user_logged_in() ) : ?>

 <?php get_template_part('navigation', 'members'); ?>
 
 <?php else: ?>
 
<?php
$item_wrap = '
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#">Navigation</a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1"><ul id="%1$s" class="%2$s">%3$s</ul>'.get_search_form(false).'</div>';

 wp_nav_menu( array(
    'theme_location'    => 'header-primary',
    'container'     => 'nav',
    'container_id'      => 'header-primary',
    'container_class'   => 'navbar navbar-default',
    'menu_class'        => 'nav navbar-nav navbar-left', 
    'echo'          => true,
    'items_wrap'        => $item_wrap,
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>
 
 <?php endif; ?>
</div>
    
        </div><!-- .col -->  
    </div><!-- .row -->
    </div><!-- .container -->
    </div><!-- #header-container -->
    



