<?php get_header(); ?>

  <div id="main-container" class="bordered">
    <div class="container">
        <div class="container-inner">
      <div class="row">
        <div class="col-md-8 main-content">
           
           <?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					
					get_template_part( 'content', get_post_format() );


					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
			?>
			
        </div>
        <div class="col-md-4 sidebar">
           <?php get_sidebar(); ?>
        </div>
      </div><!-- row -->
      </div><!-- container-inner -->
    </div><!-- container-->
   </div><!-- #main-container -->
    


<?php get_footer(); ?>
