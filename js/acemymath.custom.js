(function($){
    $(window).scroll( function() {
    
        if( ( $("body").scrollTop() > 160 ) || ( $("html").scrollTop() > 160 ) )
        {
            $('body').addClass('sticky-nav');
        } else {
            $('body').removeClass('sticky-nav');
        }   
        
        if( ( $("body").scrollTop() > 300 ) || ( $("html").scrollTop() > 300 ) )
        {
            if( $('#backToTop').length == 0 ) {
                $('body').append('<a id="backToTop" href="javascript:void(0);"><span class="glyphicon glyphicon-chevron-up"></span></a>');
                $('#backToTop').fadeIn('slow');
                $('#backToTop').click(function(event) {
                    event.preventDefault();
                    $('html, body').animate({scrollTop: 0}, 500);
                    return false;
                 });
            }
        } else {
            $('#backToTop').remove();
        }
    });
    
    $(document).ready(function() {
	    $(".fancybox").fancybox({
		    maxWidth	: 800,
		    maxHeight	: 600,
		    fitToView	: false,
		    width		: '70%',
		    height		: '70%',
		    autoSize	: false,
		    closeClick	: false,
		    openEffect	: 'none',
		    closeEffect	: 'none',
		    padding     : 0,
		    margin      : 0,
	    });
	    $('.btn-lesson-video').on('click', function() {
			var lesson_id = $(this).attr('data-id');
			$('#lesson_video_'+lesson_id).submit();
		});
	    $('.btn-lesson-exercise').on('click', function() {
			var lesson_id = $(this).attr('data-id');
			$('#lesson_exercise_'+lesson_id).submit();
		});
	    $('.btn-lesson-worksheet').on('click', function() {
			var lesson_id = $(this).attr('data-id');
			$('#lesson_worksheet_'+lesson_id).submit();
		});
		$('.btn-lesson-report').on('click', function() {
			var lesson_id = $(this).attr('data-id');
			$('#lesson_report_'+lesson_id).submit();
		});
    });

})(jQuery);
