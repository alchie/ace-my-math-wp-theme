(function ($) {
	
    Questionaire = {
        data : [],

        totalItems : 0,

        gradeLevel : 2,

        lesson : 1,

        settings : { manualExplanation: false, manualSubmit: false },

        progress : 2,

        currentIndex : 0,
        
        currentExercise : {},
        
        answer: '',
        
        checked: false,

        next : function () {
            var self = this;

            self.currentIndex += 1;

            if (self.currentIndex == self.totalItems) {
                self.currentIndex = 0;
            }

            self.currentExercise = self.data[self.currentIndex];
            
            self.display();
        },

        check : function (answer) {
            var self = this;
            
            self.checked = true;

            var correctAnswer = self.currentExercise.correctAnswer;
            
            if (answer == correctAnswer) {
                if ($('#manual-explanation').is(':checked')) {
                    self.next();
                }
                else {
                    self.showExplanation(true);
                    $('#submit').prop('disabled', true);
                }
            }
            else {
                self.showExplanation(true);
                $('#submit').prop('disabled', true);
            }
        },
        
        showExplanation : function (show) {
            var self = this;

            if (show) {
                $('.l-explanation').show();
                $('.explanation').text(self.currentExercise.explanation);
            }
            else {
                $('.l-explanation').hide();
                $('.explanation').empty();
            }
            
            $('#continue-practice').focus();
        },

        display : function () {
            var self = this;

            var exercise = self.currentExercise;
            
            self.checked = false;

            // question
            $('.question').text(exercise.question);
            
            // choices
            // clear answers area

            if (exercise.type == 'input') {
                $('#select-answer').empty();
                $('#input-answer').show();
                $('#input-answer').val('');
            }
            
            else {
                $('#input-answer').hide();
                $('#select-answer').empty();

                var choices = exercise.choices;

                for (var i = 0; i < choices.length; i++) {
                    var letter = String.fromCharCode(65 + i);
                    letter = letter + '. ';
                    $('<div><span class="l-choices">' + letter + '<span class="choices">' + choices[i] + '</span></span></div>').appendTo('#select-answer');
               }
            }

            self.showExplanation(false);
            $('#submit').prop('disabled', false);
            $('#exercises').show();
        },
        
        setupHandlers : function () {
            var self = this;

            $('#submit').click(function (e) {
                self.check(self.answer);

                e.preventDefault();
            });

            $('#continue-practice').click(function (e) {
                self.next();

                e.preventDefault();
            });

            $('#input-answer').keyup(function (e) {
                self.answer = $(this).val();
            });

            $('#select-answer').on('click', '.l-choices', function () {

                if (self.checked) {
                    return;
                }

                $('.choices').parent().removeClass('bg-primary');
                $(this).parent().addClass('bg-primary');

                self.answer = $(this).text();

                if (!$('#manual-submit').is(':checked')) {
                    self.check(self.answer);
                }
            });

        },

        load : function () {
            var self = this;
            self.setupHandlers();

            $.ajax({
                url: ajax_url,
                type: 'post',
                data: {
                    action : 'exercises',
                    file : exercise_filename,
                    questions : questions_n,
                    items : items_n
                    },
                success: function(data, status) {
                    self.data = data;
                    self.totalItems = data.length;
                    self.currentExercise = self.data[self.currentIndex];

                    self.display();
                },
                error: function(xhr, desc, err) {
                  console.log(xhr);
                  console.log("Details: " + desc + "\nError:" + err);
                }
            });
        }
    };
    
    Questionaire.load();
})(jQuery);
