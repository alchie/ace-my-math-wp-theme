<?php 

$chapter_terms = wp_get_post_terms( get_the_ID(), 'chapter', array("fields" => "ids") );

function exer_header() {
echo "<script><!--
var ajax_url = \"".get_bloginfo("home")."/wp-admin/admin-ajax.php\";
var exercise_filename = \"". get_post_meta(get_the_ID(), 'exercise_filename', true)."\";
var questions_n = \"1,2,3,4,5,6,7,8,9\";
var items_n = 20;
//--></script>";
}
add_action('wp_head', 'exer_header');

wp_enqueue_script( 'jquery.timer', plugins_url() . '/exercises/jquery.timer.js', array( 'jquery' ), '2014-05-29', true );
wp_enqueue_script( 'exercises', plugins_url() . '/exercises/js/exercise.js', array( 'jquery', 'jquery.timer' ), '2014-05-29', true );


get_header();

?>

  <div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        
<?php 
$n = 0;
$default_level = $_POST['level_id'];

$levels_allowed = AceCurrentUserLevelsAllowed();
$levels_raw = get_terms('level');
$levels = array();

foreach($levels_raw as $lvlraw) {   
    $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
    if( isset( $levels[$order]) ) {
        $levels[] = $lvlraw;
    } else {
        $levels[$order] = $lvlraw;
    }
}

ksort($levels);

if( count($levels) > 0 ) :
    foreach($levels as $level) : 
    if( is_int( array_search( $level->term_id, $levels_allowed ) ) ) {
		
	if( $default_level == 0 ) {
        $default_level = $level->term_id;
    }
		
?>
          <li <?php echo ($default_level==$level->term_id) ? 'class="active"' : ''; ?>><a href="<?php echo get_term_link( $level, 'level' ); ?>"><?php echo $level->name; ?></a></li>
         
<?php 
}
$n++;
    endforeach;
endif;
?>

        </ul>
        
	
	
		</div>
      <div class="col-md-11">
 
    <div class="whitebox add-padding" style="min-height:500px;">
        
<h4><strong></string>Lesson <?php echo $post->menu_order; ?>:</strong> <?php the_title(); ?> - Practice Exercises</h4>

 <div class="btn-group btn-group-sm btn-group-justified btn-group-actions">
  <span class="btn btn-primary first btn-lesson-video" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-film"></span> Video Lesson
  <?php if($_POST['action'] != 'video') { ?>
  <form action="<?php echo get_permalink(); ?>?show=video" method="post" id="lesson_video_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="video">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-danger btn-lesson-exercise" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-pencil"></span> Practice
  <?php if($_POST['action'] != 'exercise') { ?>
   <form action="<?php echo get_permalink(); ?>?show=exercise" method="post" id="lesson_exercise_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="exercise">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary btn-lesson-worksheet" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-list-alt"></span> Worksheets
  <?php if($_POST['action'] != 'worksheet') { ?>
     <form action="<?php echo get_permalink(); ?>?show=worksheet" method="post" id="lesson_worksheet_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="worksheet">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary last btn-lesson-report" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-signal"></span> Report
  <?php if($_POST['action'] != 'report') { ?>
     <form action="<?php echo get_permalink(); ?>?show=report" method="post" id="lesson_report_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="report">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
</div>

<div id="ajax-loader"><img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></div>

 <div class="row" id="exercises" style="display:none">
            <div class="col-md-10 col-md-offset-1">
				
				<div id="alert"></div>
				
				<div class="pull-right">
						 <strong>Total Time:</strong> <span id="exercise-total-time">00:00:00</span><br>
	 <strong>Answer Time:</strong> <span id="exercise-timer">0</span> seconds
				</div>
				
                <h2 class="question"></h2>
                <p class="instruction"></p>
                <div class="form-group" id="answers">
                    <div id="select-answer"></div>
                </div>
                <div class="row">
					<div class="col-md-12 form-group">
						<button DISABLED id="submit" class="btn btn-warning">Submit</button>
					</div>
                </div>
                <div>
                    <div class="checkbox">
                        <label>
                            <input id="manual-explanation" type="checkbox" value=""/>Show explanation when answered wrongly
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="manual-submit" type="checkbox" value=""/>Click button or hit enter to submit
                        </label>
                    </div>
                </div>
                <div id="the-answers" class="row" style="display:none">
					<div class="col-md-6">
						<h1 class="text-center">Correct Answer</h1>
						<h3 class="correct-answer text-center"></h3>
					</div>
					<div class="col-md-6">
						<h1 class="text-center">Your Answer</h1>
						<h3 class="your-answer text-center"></h3>
						<span class="glyphicon"></span>
					</div>
                </div>
                <div class="row l-explanation bg-info">
                    <p class="explanation">explanation</p>
                    <div>
						
                        <button id="watch-video" data-id="<?php the_ID(); ?>" class="btn btn-info btn-lesson-video">Watch video again</button>
                        <button id="continue-practice" class="btn btn-success">Continue practice</button>
                    </div>
                </div>
            </div>
        </div>

        
    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
    
      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->



<?php get_footer(); ?>
