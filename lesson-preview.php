<?php 

$lessons_allowed = ( is_user_logged_in() ) ? AceCurrentUserLessonsAllowed() : AceGuestLessonsAllowed();
$lesson_restricted = ( array_search( get_the_ID(), $lessons_allowed) === FALSE );

get_header(); ?>

<?php
while ( have_posts() ) : the_post();
				
?>
  <div id="main-container" class="single-lesson">
    <div class="container">


        <div class="row">
             <div class="col-md-12">
    <?php
		// Page thumbnail and title.
		the_title( '<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->' );
	?>
            </div>
        </div>
        
      <div class="row">
      

	
        <div class="col-md-8">
            <div class="main-content whitebox">
           



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<div class="entry-content">
		<?php
			the_content();

		?>

<?php if( ! $lesson_restricted ) { ?>
		<form action="<?php the_permalink(); ?>" method="post">
		<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
		<input type="hidden" name="action" value="start_lesson">
		<?php wp_nonce_field('start_lesson_' . get_the_ID(),'lesson_nonce'); ?>
		<button type="submit" class="btn btn-success btn-lg btn-block">Start Lesson</button>
		</form>
<?php } else { ?>		
    <a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>" class="btn btn-success btn-lg btn-block">Signup Now</a>
<?php } ?>
	</div><!-- .entry-content -->
	
	
	
</article><!-- #post-## -->


			</div>
        </div>
        <div class="col-md-4">
            <p><a href="<?php echo get_permalink(); ?>?start=problems" class="btn btn-danger btn-lg btn-block">Practice Problems</a></p>
            <p> <a href="<?php echo get_permalink(); ?>?start=worksheet" class="btn btn-danger btn-lg btn-block">Worksheet</a></p>
             
            <?php get_sidebar('lessons'); ?>
           
        </div>
      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->
<?php endwhile;	?>    


<?php get_footer(); ?>
