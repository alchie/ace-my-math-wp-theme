<?php 

$chapter_terms = wp_get_post_terms( get_the_ID(), 'chapter', array("fields" => "ids") );
get_header();

?>

  <div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        
<?php 
$n = 0;
$default_level = $_POST['level_id'];

$levels_allowed = AceCurrentUserLevelsAllowed();
$levels_raw = get_terms('level');
$levels = array();

foreach($levels_raw as $lvlraw) {   
    $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
    if( isset( $levels[$order]) ) {
        $levels[] = $lvlraw;
    } else {
        $levels[$order] = $lvlraw;
    }
}

ksort($levels);

if( count($levels) > 0 ) :
    foreach($levels as $level) : 
    if( is_int( array_search( $level->term_id, $levels_allowed ) ) ) {
		
	if( $default_level == 0 ) {
        $default_level = $level->term_id;
    }
		
?>
          <li <?php echo ($default_level==$level->term_id) ? 'class="active"' : ''; ?>><a href="<?php echo get_term_link( $level, 'level' ); ?>"><?php echo $level->name; ?></a></li>
         
<?php 
}
$n++;
    endforeach;
endif;
?>

        </ul>
        
	
	
		</div>
      <div class="col-md-11">
 
    <div class="whitebox add-padding" style="min-height:500px;">
        
<h4><strong></string>Lesson <?php echo $post->menu_order; ?>:</strong> <?php the_title(); ?> - Report</h4>

 <div class="btn-group btn-group-sm btn-group-justified btn-group-actions">
  <span class="btn btn-primary first btn-lesson-video" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-film"></span> Video Lesson
  <?php if($_POST['action'] != 'video') { ?>
  <form action="<?php echo get_permalink(); ?>?show=video" method="post" id="lesson_video_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="video">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary btn-lesson-exercise" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-pencil"></span> Practice
  <?php if($_POST['action'] != 'exercise') { ?>
   <form action="<?php echo get_permalink(); ?>?show=exercise" method="post" id="lesson_exercise_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="exercise">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary btn-lesson-worksheet" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-list-alt"></span> Worksheets
  <?php if($_POST['action'] != 'worksheet') { ?>
     <form action="<?php echo get_permalink(); ?>?show=worksheet" method="post" id="lesson_worksheet_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="worksheet">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-danger last btn-lesson-report" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-signal"></span> Report
  <?php if($_POST['action'] != 'report') { ?>
     <form action="<?php echo get_permalink(); ?>?show=report" method="post" id="lesson_report_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="report">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
</div>

 <div class="btn-group btn-group-sm btn-group-justified btn-group-actions">
     <div class="btn btn-default first"><span class="glyphicon glyphicon-check"></span> Recent Activities</div>
     <div class="btn btn-default"><span class="glyphicon glyphicon-signal"></span> Skills Improved</div>
     <div class="btn btn-default"><span class="glyphicon glyphicon-record"></span> Trouble Spots</div>
     <div class="btn btn-default"><span class="glyphicon glyphicon-certificate"></span> Congratulations</div>
     <div class="btn btn-default last"><span class="glyphicon glyphicon-screenshot"></span> One Week Goal</div>
</div>
        
    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
    
      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->



<?php get_footer(); ?>
