<?php 

$quizzes = get_post_meta(get_the_ID(), 'lesson_video_quizzes', true);
$chapter_terms = wp_get_post_terms( get_the_ID(), 'chapter', array("fields" => "ids") );

function scripttofooter() {
    global $current_user;
    get_currentuserinfo();

    $taken = aceLessonProgressRetrieve($current_user->ID, get_the_ID());
    $quizzes = get_post_meta(get_the_ID(), 'lesson_video_quizzes', true);
    ?>
    <script type="text/javascript">
    (function($) {
    var paused = 0;
    var occurrence = 0;
    var evt_pos = 0;
    var maxPosition = 0;
    var tries = 1;
    var seeking = false;
    jwplayer("lessonVideoPlayer").setup({
        file: "<?php echo get_post_meta( get_the_ID() , 'lesson_video_url', true); ?>",
        height: 550,
        width: 717,
        autostart: true,
        allowfullscreen : false,
        icons: false,
        skin: "<?php echo get_template_directory_uri(); ?>/skin/five/five.xml",
        events: {
            onTime : function(event) {
               pos = Math.round( event.position );
<?php if( count( $taken ) == 0 ) { ?>
               if ( seeking === false ) {
                    maxPosition = Math.max(event.position, maxPosition);
               }
<?php } ?>
               if( evt_pos == pos ) {
                    occurrence = occurrence + 1;
               }
               if( pos != evt_pos ) {
                    evt_pos = pos;
                    occurrence = 0;
               }
       
<?php 
$index = 0;
if( ($quizzes) && count( $quizzes ) > 0) {
foreach( $quizzes as $quiz ) { 
$minutes = $seconds = 0;
$str_time = $quiz['popupTime'];
sscanf($str_time, "%d:%d", $minutes, $seconds);
$time_seconds = ($minutes * 60) + $seconds;

if( $time_seconds > 0 ) {
    ?>
              if( (evt_pos - 1) == parseInt(<?php echo round($time_seconds); ?>) && occurrence == 0 ) {
                            jwplayer().pause();
                            $('#overlayContainerbg').show();
                            $('#overlayContainer').show();
                            $('#overlayContainer .inner').hide();                            
                            $('#overlayContainer #quiz-overlay-<?php echo $index; ?>').show();              
               }
              
<?php 
}
$index++;
} 
} 
?>
               if( event.position == event.duration ) {
                    $.ajax({
                        type : 'POST',
                        url : '<?php echo admin_url("admin-ajax.php"); ?>',
                        dataType : 'text',
                        data : {
                            action : 'video_finished', 
                            lesson_id : '<?php the_ID(); ?>'
                        }
                    }).done(function(msg) {
                        alert(msg);
                    });
                    
               }
            },
<?php if( count( $taken ) == 0 ) { ?>
            onSeek : function(event) {
                if( seeking === false ) {
 
                    if( event.offset > maxPosition ) {
                       seeking = true;
                       jwplayer().pause();
                       setTimeout(function ()
                        {  
                           jwplayer().seek(maxPosition);
                        }, 50);
                    }
                } else {
                    seeking = false;
                }
            },
<?php } ?>     
/*       
            onPause : function(oldstate) {
                if( seeking === false ) {
                    $('#overlayContainerbg').css('opacity', 1).show();
                    $('#overlayContainer').show();
                    $('#overlayContainer .welcome').show();   
                } else {
                    $('#overlayContainerbg').css('opacity', 0).show();
                }                  
            },
           
            onPlay : function() {
                    $('#overlayContainerbg').hide();
                    $('#overlayContainer').hide();
                    $('#overlayContainer .inner').hide();
            }
*/ 
        }
    });
    
    $(document).ready(function() {
        $('.playVideo').click(function() { 
            $('#overlayContainerbg').hide();
            $('#overlayContainer').hide();
            $('#overlayContainer .inner').hide();
            jwplayer("lessonVideoPlayer").play();
        });
        
        $('.isCorrect').click(function() { 
            $(this).prop('disabled', true).removeClass('btn-default').addClass('btn-success');
            $('.isWrong').prop('disabled', true);
            $('.quiz-alert').text('Correct!').removeClass('alert-danger').addClass('alert-success').show();            
            $('#overlayContainerbg').delay(800).hide(function() {
                    $('#overlayContainer').hide();
                    $('.quiz-alert').hide();
                    $('.btn-choices').prop('disabled', false).removeClass('btn-danger').removeClass('btn-success').addClass('btn-default');
                    $('#overlayContainer .inner').hide();
                    jwplayer("lessonVideoPlayer").play();
                    tries = 1;
            });
        });
        
        $('.isWrong').click(function() { 
            $(this).prop('disabled', true).removeClass('btn-default').addClass('btn-danger');
            $('.quiz-alert').text('Try Again! - Tries: ' + tries).removeClass('alert-success').addClass('alert-danger').show();
            tries++;
        });
        
        $('.submit-input-answer').click(function() {           
            var data_index = $(this).attr('data-index'), wrong = false;
            $('.answer-' + data_index).each(function() {
                var answer = $(this).attr('data-answer'), value = $(this).val();
                    if( answer.trim().replace(/\s+/g, '').replace(/,/g, '') != value.trim().replace(/\s+/g, '').replace(/,/g, '') ) {
                            $(this).css('border-color', 'red');
                            return wrong = true;
                    } else {
                            $(this).css('border-color', 'green');
                    }
            });
            
            if( wrong ) {
                $('.quiz-alert').text('Try Again! - Tries: ' + tries).removeClass('alert-success').addClass('alert-danger').show();
                tries++;
            } else {
                $('.quiz-alert').text('Correct!').removeClass('alert-danger').addClass('alert-success').show(); 
                $('.answer-' + data_index).prop('disabled', true);
                $('#overlayContainerbg').delay(800).hide(function() {
                    $('#overlayContainer').hide();
                    $('.quiz-alert').hide();
                    $('#overlayContainer .inner').hide();
                    $('.answer-' + data_index).prop('disabled', false).val('').css('border-color', '');
                    jwplayer("lessonVideoPlayer").play();
                    tries = 1;
                    
            });
            }
        });
        
        $('.check-input-answer').click(function() {           
            var data_index = $(this).attr('data-index'), wrong = false;
            $('.answer-' + data_index).each(function() {
                var answer = $(this).attr('data-answer'), value = $(this).val(), n = $(this).attr('data-n');
                
                    if( answer.trim().replace(/\s+/g, '').replace(/,/g, '') != value.trim().replace(/\s+/g, '').replace(/,/g, '') )  {
                            if( $(this).prop('checked') ) {
								wrong = true;
							}
                    } else {
							if( $(this).prop('checked') == false ) {
								wrong = true;
							}
					}
            });
            if( wrong ) {
                $('.quiz-alert').text('Try Again! - Tries: ' + tries).removeClass('alert-success').addClass('alert-danger').show();
                tries++;
            } else {
                $('.quiz-alert').text('Correct!').removeClass('alert-danger').addClass('alert-success').show(); 
                $('.answer-' + data_index).prop('disabled', true);
                $('#overlayContainerbg').delay(800).hide(function() {
                    $('#overlayContainer').hide();
                    $('.quiz-alert').hide();
                    $('#overlayContainer .inner').hide();
                    $('.answer-' + data_index).prop('disabled', false);
                    $('.answer-' + data_index).prop('checked', false);
                    jwplayer("lessonVideoPlayer").play();
                    tries = 1;
                    
            });
            }
        });
            
    });
    })(jQuery);
</script>
    <?php
}
add_action('wp_footer', 'scripttofooter'); 

get_header();
?>

  <div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        
<?php 
$n = 0;
$default_level = $_POST['level_id'];

$levels_allowed = AceCurrentUserLevelsAllowed();
$levels_raw = get_terms('level');
$levels = array();

foreach($levels_raw as $lvlraw) {   
    $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
    if( isset( $levels[$order]) ) {
        $levels[] = $lvlraw;
    } else {
        $levels[$order] = $lvlraw;
    }
}

ksort($levels);

if( count($levels) > 0 ) :
    foreach($levels as $level) : 
    if( is_int( array_search( $level->term_id, $levels_allowed ) ) ) {
		
	if( $default_level == 0 ) {
        $default_level = $level->term_id;
    }
		
?>
          <li <?php echo ($default_level==$level->term_id) ? 'class="active"' : ''; ?>><a href="<?php echo get_term_link( $level, 'level' ); ?>"><?php echo $level->name; ?></a></li>
         
<?php 
}
$n++;
    endforeach;
endif;
?>

        </ul>
        
	
	
		</div>
      <div class="col-md-8">
 
    <div class="whitebox add-padding" style="min-height:500px;">
        
<h4><strong></string>Lesson <?php echo $post->menu_order; ?>:</strong> <?php the_title(); ?> - Video Lesson</h4>

 <div class="btn-group btn-group-sm btn-group-justified btn-group-actions">
  <span class="btn btn-danger first btn-lesson-video" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-film"></span> Video Lesson
  <?php if($_POST['action'] != 'video') { ?>
  <form action="<?php echo get_permalink(); ?>?show=video" method="post" id="lesson_video_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="video">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary btn-lesson-exercise" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-pencil"></span> Practice
  <?php if($_POST['action'] != 'exercise') { ?>
   <form action="<?php echo get_permalink(); ?>?show=exercise" method="post" id="lesson_exercise_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="exercise">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary btn-lesson-worksheet" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-list-alt"></span> Worksheets
  <?php if($_POST['action'] != 'worksheet') { ?>
     <form action="<?php echo get_permalink(); ?>?show=worksheet" method="post" id="lesson_worksheet_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="worksheet">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
  <span class="btn btn-primary last btn-lesson-report" data-id="<?php the_ID(); ?>"><span class="glyphicon glyphicon-signal"></span> Report
  <?php if($_POST['action'] != 'report') { ?>
     <form action="<?php echo get_permalink(); ?>?show=report" method="post" id="lesson_report_<?php the_ID(); ?>" class="hidden">
			<input type="hidden" name="lesson_id" value="<?php the_ID(); ?>">
			<input type="hidden" name="chapter_id" value="<?php echo implode(',',$chapter_terms); ?>">
			<input type="hidden" name="level_id" value="<?php echo $default_level; ?>">
			<input type="hidden" name="action" value="report">
				<?php wp_nonce_field('start_lesson_' . get_the_ID(), 'lesson_nonce'); ?>
			</form><?php } ?>
  </span>
</div>


<div id="videoContainer">
<div id="lessonVideoPlayer">Loading the player...</div>
<div id="overlayContainerbg" style="display:none"></div>
<div id="overlayContainer" style="display:none">
<div class="quiz-alert alert alert-default" style="display:none;">Correct!</div>
    <div class="welcome inner">
        <h1><?php the_title(); ?></h1>
        <button class="btn btn-success playVideo">Play Video</button>
    </div>
<?php 
$index = 0;
if( ($quizzes) && count( $quizzes ) > 0) {
foreach( $quizzes as $quiz ) { ?>
    <div id="quiz-overlay-<?php echo $index; ?>"class="inner" style="display:none">
        <h2><?php echo $quiz['popupTitle']; ?></h2>
        
<?php 
switch($quiz['input_type']) { ?>
<?php
    default:
    case 'choices': ?>
        <?php if( $quiz['choices'] ) { 
			$n = 0;
        foreach( $quiz['choices'] as $choice ) { 
        $is_correct = ( isset($choice['correct']) ) ? 'isCorrect' : 'isWrong'; 
        ?>
        <?php if( $quiz['choices_type'] == 'multiple') { ?>
            <p class="form-group choice-n-<?php echo $n; ?>"><label><input type="checkbox" class="answer-<?php echo $index; ?>" value="<?php echo ( isset($choice['correct']) ) ? '1' : '0'; ?>" data-answer="1" data-n="<?php echo $n; ?>"> <?php echo $choice['choice']; ?></label></p>
         <?php } else { ?>
            <p class="choice-n-<?php echo $n; ?>"><button class="btn btn-default btn-sm btn-choices <?php echo $is_correct; ?>"><?php echo $choice['choice']; ?></button></p>
           <?php } ?>
        <?php $n++; } ?>
        <?php if( $quiz['choices_type'] == 'multiple') { ?> 
			<button class="btn btn-success check-input-answer" data-type="checkbox" data-index="<?php echo $index; ?>">Submit</button>
		<?php } ?>
        <?php } else { ?>
            <button class="btn btn-success playVideo">Continue</button>
        <?php } ?>

<?php break; ?>
<?php case 'input': ?>
<div class="row">
    
    <?php if( $quiz['input'] ) { 
        foreach( $quiz['input'] as $input ) { 
            switch($input['type']) {
                case 'input':
                    echo '<div class="col-md-2 input-input"><input type="text" class="form-control text-center answer-'.$index.'" data-answer="'.$input['answer'].'"></div>';
                break;
                case 'label':
                    echo '<div class="col-md-2 input-label text-center">'.$input['label'].'</div>';
                break;
                case 'dropdown':
                    echo '<div class="col-md-2 input-input"><select class="form-control answer-'.$index.'" data-answer="'.$input['answer'].'">';
                    foreach( $input['choices'] as $ddchoice ) {
                        echo '<option value="'.$ddchoice['option'].'">'.$ddchoice['option'].'</option>';
                    }
                    echo '</select></div>';
                break;
            }
         }
         echo '<br><br><button class="btn btn-success submit-input-answer" data-index="'.$index.'">Submit</button>'; 
        } else {
         echo '<button class="btn btn-success playVideo">Continue</button>';
} ?>
    
</div>

<?php break; ?>
<?php } ?>
    </div>
<?php 
$index++;
}
} ?>
</div>
</div>
  
  
  
        
    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            <?php get_sidebar('testimonials'); ?>
            
           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->



<?php get_footer(); ?>
