<?php 

$quizzes = get_post_meta(get_the_ID(), 'lesson_video_quizzes', true);

function scripttofooter() {
    global $current_user;
    get_currentuserinfo();

    $taken = aceLessonProgressRetrieve($current_user->ID, get_the_ID());
    $quizzes = get_post_meta(get_the_ID(), 'lesson_video_quizzes', true);
    ?>
    <script type="text/javascript">
    (function($) {
    var paused = 0;
    var occurrence = 0;
    var evt_pos = 0;
    var maxPosition = 0;
    var seeking = false;
    jwplayer("lessonVideoPlayer").setup({
        file: "<?php echo get_post_meta( get_the_ID() , 'lesson_video_url', true); ?>",
        height: 550,
        width: 1140,
        autostart: true,
        events: {
            onTime : function(event) {
               pos = Math.round( event.position );
               
<?php if( count( $taken ) == 0 ) { ?>
               if ( seeking === false ) {
                    maxPosition = Math.max(event.position, maxPosition);
               }
<?php } ?>
               if( evt_pos == pos ) {
                    occurrence = occurrence + 1;
               }
               if( pos != evt_pos ) {
                    evt_pos = pos;
                    occurrence = 0;
               }
       
<?php 
$index = 0;
if( ($quizzes) && count( $quizzes ) > 0) {
foreach( $quizzes as $quiz ) { ?>
              if( (evt_pos - 1) == parseInt(<?php echo round($quiz['popupTime']); ?>) && occurrence == 0 ) {
                            jwplayer().pause();
                            $('#overlayContainerbg').show();
                            $('#overlayContainer').show();
                            $('#overlayContainer .inner').hide();                            
                            $('#overlayContainer #quiz-overlay-<?php echo $index; ?>').show();              
               }
              
<?php 
$index++;
} 
} 
?>
               if( event.position == event.duration ) {
                    $.ajax({
                        type : 'POST',
                        url : '<?php echo admin_url("admin-ajax.php"); ?>',
                        dataType : 'text',
                        data : {
                            action : 'video_finished', 
                            lesson_id : '<?php the_ID(); ?>'
                        }
                    }).done(function(msg) {
                        alert(msg);
                    });
                    
               }
            },
<?php if( count( $taken ) == 0 ) { ?>
            onSeek : function(event) {
                if( seeking === false ) {
 
                    if( event.offset > maxPosition ) {
                       seeking = true;
                       jwplayer().pause();
                       setTimeout(function ()
                        {  
                           jwplayer().seek(maxPosition);
                        }, 50);
                    }
                } else {
                    seeking = false;
                }
            },
<?php } ?>            
            onPause : function(oldstate) {
                if( seeking === false ) {
                    $('#overlayContainerbg').css('opacity', 1).show();
                    $('#overlayContainer').show();
                    $('#overlayContainer .welcome').show();   
                } else {
                    $('#overlayContainerbg').css('opacity', 0).show();
                }                  
            },
            
            onPlay : function() {
                    $('#overlayContainerbg').hide();
                    $('#overlayContainer').hide();
                    $('#overlayContainer .inner').hide();
            }
        }
    });
    
    $(document).ready(function() {
        $('.playVideo').click(function() { 
            $('#overlayContainerbg').hide();
            $('#overlayContainer').hide();
            $('#overlayContainer .inner').hide();
            jwplayer("lessonVideoPlayer").play();
        });
        
        $('.isCorrect').click(function() { 
            $(this).prop('disabled', true).removeClass('btn-default').addClass('btn-success');
            $('.quiz-alert').text('Correct!').removeClass('alert-danger').addClass('alert-success').show();            
            $('#overlayContainerbg').delay(800).hide(function() {
                    $('#overlayContainer').hide();
                    $('.quiz-alert').hide();
                    $('.btn-choices').prop('disabled', false).removeClass('btn-danger').removeClass('btn-success').addClass('btn-default');
                    $('#overlayContainer .inner').hide();
                    jwplayer("lessonVideoPlayer").play();
            });
        });
        
        $('.isWrong').click(function() { 
            $(this).prop('disabled', true).removeClass('btn-default').addClass('btn-danger');
            $('.quiz-alert').text('Try Again!').removeClass('alert-success').addClass('alert-danger').show();
            //$('#overlayContainerbg').hide();
           // $('#overlayContainer').hide();
            //$('#overlayContainer .inner').hide();
            //jwplayer("lessonVideoPlayer").play();
        });
        
    });
    })(jQuery);
</script>
    <?php
}
add_action('wp_footer', 'scripttofooter'); 

get_header();
?>

<div class="container">
<div class="row">
    <div class="col-md-12">
    

<div id="videoContainer">
<div id="lessonVideoPlayer">Loading the player...</div>
<div id="overlayContainerbg" style="display:none"></div>
<div id="overlayContainer" style="display:none">
<div class="quiz-alert alert alert-default" style="display:none;">Correct!</div>
    <div class="welcome inner">
        <h1><?php the_title(); ?></h1>
        <button class="btn btn-success playVideo">Play Video</button>
    </div>
<?php 
$index = 0;
if( ($quizzes) && count( $quizzes ) > 0) {
foreach( $quizzes as $quiz ) { ?>
    <div id="quiz-overlay-<?php echo $index; ?>"class="inner" style="display:none">
        <h2><?php echo $quiz['popupTitle']; ?></h2>
        <?php if( $quiz['choices'] ) { 
        foreach( $quiz['choices'] as $choice ) { 
        $is_correct = ( isset($choice['correct']) ) ? 'isCorrect' : 'isWrong'; 
        ?>
            <p><button class="btn btn-default btn-sm btn-choices <?php echo $is_correct; ?>"><?php echo $choice['choice']; ?></button></p>
        <?php } 
        } else {
        ?>
            <button class="btn btn-success playVideo">Continue</button>
        <?php } ?>
    </div>
<?php 
$index++;
}
} ?>
</div>


</div>
    </div>
</div>
</div>

<?php get_footer(); ?>
