<?php
$logout = wp_logout_url();
$membership_options = get_option('membership_options');
$account = get_permalink( $membership_options['account_page'] );

global $post;

$active='';
if($post->ID == $membership_options['account_page']) {
    $active = 'class="active"';
}

if( get_user_role() == 'parent' ) {
    $theme_location = 'header-primary-parent';
} elseif( get_user_role() == 'student' ) {
    $theme_location = 'header-primary-student';
} elseif( get_user_role() == 'administrator' || get_user_role() == 'editor' ) {
    $theme_location = 'header-primary-members';    
} else {
    $theme_location = 'header-primary';
}

$item_wrap = <<<NAV
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#">Navigation</a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1"><ul id="%1\$s" class="%2\$s">%3\$s</ul>
  <ul class="nav navbar-nav navbar-right">
    <li $active><a href="{$account}">My Account</a></li>
    <li><a href="{$logout}">Logout</a></li>    
  </ul></div>
NAV;

 wp_nav_menu( array(
    'theme_location'    => $theme_location,
    'container'     => 'nav',
    'container_id'      => 'header-primary',
    'container_class'   => 'navbar navbar-default',
    'menu_class'        => 'nav navbar-nav navbar-left', 
    'echo'          => true,
    'items_wrap'        => $item_wrap,
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>
