<?php 
    if ( is_user_logged_in() ) {
        header('location: ' . home_url() );
        exit;
    }
    
?>

<?php get_header(); ?>
<div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
      <form name="loginform" id="loginform" class="login-container" action="<?php echo wp_lostpassword_url(); ?>" method="post">
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><?php the_title(); ?></h3>
  </div>
  <div class="panel-body">
   
   

  <div class="form-group">
    <label for="username">Username or E-mail:</label>
    <input type="text" name="user_login" class="form-control" id="username" placeholder="Please enter your username or email address." required>
  </div>

 <input type="hidden" name="redirect_to" value="<?php bloginfo('home'); ?>" />
		    <input type="hidden" name="testcookie" value="1" />
		    
   
  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Retreive Password</button></div>
</div>
</form>
<a href="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>" class="pull-right">Login</a>
<a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>">Create New Account</a> 
</div>
</div>
</div>
<?php get_footer(); ?>
