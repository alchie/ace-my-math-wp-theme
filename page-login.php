<?php 
    if ( is_user_logged_in() ) {
        header('location: ' . home_url() );
        exit;
    }
    $redirect = (isset($_GET['redirect'])) ? $_GET['redirect'] : get_bloginfo('home');
?>

<?php get_header(); ?>

<div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
      <form name="loginform" id="loginform" class="login-container" action="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>" method="post">
       
<?php if( isset( $_REQUEST['error'] ) ) {  ?>
<div class="alert alert-danger">
  <strong>Error!</strong> Unable to login!
</div>
<?php } ?>
<?php if( isset( $_REQUEST['notactivated'] ) ) {  ?>
<div class="alert alert-danger">
  <strong>Urgent!</strong> Student account not yet activated!
</div>
<?php } ?>
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><?php the_title(); ?></h3>
  </div>
  <div class="panel-body">
   
            <?php wp_nonce_field( 'login_ace_account' ); ?>
            <input type="hidden" name="action" value="login_ace_account" />

  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="username" class="form-control" id="username" placeholder="Enter username">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
  </div>

 <input type="hidden" name="redirect_to" value="<?php echo $redirect; ?>" />
		    <input type="hidden" name="testcookie" value="1" />
		    
   
  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Login</button></div>
</div>
</form>
<a href="<?php echo get_permalink( get_page_by_path( 'forgotpassword' ) ); ?>" class="pull-right">Forgot Password?</a>
<a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>">Create New Account</a> 
</div>
</div>
</div>
<?php get_footer(); ?>
