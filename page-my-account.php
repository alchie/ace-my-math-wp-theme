<?php 

if ( ! is_user_logged_in() ) {
    header('location: /'); 
    exit;
}

global $current_user;
get_currentuserinfo();

get_header(); 

?>


  <div id="main-container" class="registration">
    <div class="container">

 <div class="row">
             <div class="col-md-12">
             
                 <header class="entry-header">
                
                    <h1 class="entry-title">My Account</h1>
                
                 </header>
                 
            </div>

        </div>
        
      <div class="row">

        <div class="col-md-8">

            <div class="main-content whitebox">
           <div class="entry-content">
           
           <?php if( $GLOBALS['my_account_update'] ) { ?>
           <div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Success!</strong> Your personal info updated!  
  <?php if( $GLOBALS['my_account_password_update'] ) { ?>
   <br /> <strong>Password Changed!</strong> 
  <?php } ?>
</div>
           <?php } ?>
           
 
           
           
           
<form role="form" method="post">
<?php wp_nonce_field('update_my_account_' . $current_user->ID ); ?>
<input type="hidden" name="action" value="update_my_account" />
<input type="hidden" name="user_id" value="<?php echo $current_user->ID; ?>">
    <h3>Account Details</h3>
<?php if( get_user_role() == 'student') {
$current_student_gradelevel = get_term_by('ids', get_user_meta($current_user->ID, '_gradelevel', true), 'level');
?>
   <div class="form-group">
    <label for="gradelevel">Grade Level</label>
    <input type="text" class="form-control" id="gradelevel" READONLY value="<?php echo $current_student_gradelevel->name; ?>">
  </div>    
<?php } ?>
   <div class="form-group">
    <label for="username">Username</label>
    <input type="text" class="form-control" id="username" READONLY value="<?php echo $current_user->user_login; ?>">
  </div>
   <div class="form-group">
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" READONLY value="<?php echo $current_user->user_email; ?>">
  </div>
  <h3>Personal Information</h3>
  <div class="form-group">
    <label for="firstname">First Name</label>
    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter first name" value="<?php echo $current_user->user_firstname; ?>">
  </div>
  <div class="form-group">
    <label for="lastname">Last Name</label>
    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter last name" value="<?php echo $current_user->user_lastname; ?>">
  </div>
    <div class="form-group">
    <label for="nickname">Nick Name</label>
    <input type="text" class="form-control" id="nickname" name="nickname" placeholder="Enter nick name" value="<?php echo $current_user->nickname; ?>">
  </div>
  <div class="form-group">
    <label for="displayname">Display Name</label>
    <input type="text" class="form-control" id="displayname" name="displayname" placeholder="Enter nick name" value="<?php echo $current_user->display_name; ?>">
  </div>
  <h3>Change Password <?php echo ( get_user_role() == 'student' ) ? '<small>Note: Your parent can change your password.</small>' : ''; ?></h3>
  
    <div class="form-group">
    <label for="newpassword">New Password</label>
    <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="Enter new password">
  </div>
    <div class="form-group">
    <label for="confirmpassword">Confirm Password</label>
    <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm New Password">
  </div>
  <button type="submit" class="btn btn-success btn-sm">Update</button>
</form>
</div>


			</div>
			
        </div>
        
        <div class="col-md-4">
        
            <div class="sidebar whitebox">
            
            <?php get_sidebar('testimonials'); ?>
            
           </div>
           
        </div>
        
      </div><!-- row -->

    </div><!-- container-->
    
   </div><!-- #main-container -->
   

<?php get_footer(); ?>
