<?php 

if (  ! is_user_logged_in() ) {
    header('location: ' . home_url('login') . '?redirect=' . urlencode( get_permalink( get_page_by_path( 'my-students' ) ) ) );
    exit;
}

if( get_user_role() != 'parent' ) {
    header('location: /');
    exit;
}

global $current_user;
get_currentuserinfo();

if( isset($_GET['uid']) && $_GET['uid'] != '' ) {

    $current_student_gradelevel = get_user_meta($_GET['uid'], '_gradelevel', true);
    $current_student_emailalerts = get_user_meta($_GET['uid'], '_emailalerts', true);
    
    $current_student_data = get_user_by( 'id', $_GET['uid'] ); 
}

if( isset($_GET['action']) && ( $_GET['action'] == 'addnew' || $_GET['action'] == 'manage' ) ) {
// set levels
    $levels_raw = get_terms('level', array( 'hide_empty' => false));
    $levels = array();

    foreach($levels_raw as $lvlraw) {   
        $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
        if( isset( $levels[$order]) ) {
            $levels[] = $lvlraw;
        } else {
            $levels[$order] = $lvlraw;
        }
    }

    ksort($levels);

}

get_header(); ?>


  <div id="main-container">
    <div class="container">

      <div class="row">
      
         <div class="col-md-12">

<?php switch( $_GET['action'] ) { ?>
<?php case 'addnew' : ?>

<div style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
<?php if( isset( $GLOBALS['add_ace_student_error'] ) ) {  ?>
<div class="alert alert-danger">
  <strong><?php foreach( $GLOBALS['add_ace_student_error'] as $error ) {
  echo array_shift( $error );
  } ?></strong>
</div>
<?php } ?>


   <form name="addnewstudentform" id="addnewstudentform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Add New Student</h3>
  </div>
  <div class="panel-body">
   
            <?php wp_nonce_field( 'add_ace_student_' . $current_user->ID  ); ?>
            <input type="hidden" name="action" value="add_ace_student" />

 <div class="form-group">
    <label for="gradelevel">Grade Level</label>

    <select name="gradelevel" class="form-control">
        <option value="">- - Select Grade Level - - </option>
    <?php foreach($levels as $level) { ?>
        <option value="<?php echo $level->term_id; ?>" <?php echo ( isset($_POST['gradelevel']) &&  ($_POST['gradelevel'] == $level->term_id) ) ? 'SELECTED' : ''; ?>><?php echo $level->name; ?></option>
    <?php } ?>
    </select>

  </div>
<div class="form-group">
    <label for="name">Student Name</label>
    <input type="text" name="display_name" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo (isset($_POST['display_name'])) ? $_POST['display_name'] : ''; ?>">
  </div>
  <div class="form-group">
    <label for="email">Student Email</label>
    <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo (isset($_POST['email'])) ? $_POST['email'] : ''; ?>">
  </div>
    <div class="form-group">
    <label for="username">Student Username</label>
    <input type="text" name="username" class="form-control" id="username" placeholder="Enter Username" value="<?php echo (isset($_POST['username'])) ? $_POST['username'] : ''; ?>">
  </div>
      <div class="form-group">
    <label for="password">Student Password</label>
    <input type="text" name="password" class="form-control" id="password" placeholder="Enter Password" value="<?php echo (isset($_POST['password'])) ? $_POST['password'] : ''; ?>">
  </div>

		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.addnewstudentform.submit();">Add</a>
        <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>" class="btn btn-danger">Cancel</a>
   </div>
   </div>
</div>
</form>

</div>
</div>
</div>




<?php break; ?>
<?php case 'manage' : ?>

<div style="margin-top:50px;">

<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
    
    <?php if( isset( $GLOBALS['manage_student_updated'] ) ) {  ?>
<div class="alert alert-success">
  <strong>Successfully Updated!</strong>
</div>
<?php } ?>

<form name="studentform" id="studentform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Manage Student : <strong><?php echo $current_student_data->user_login; ?></strong></h3>
  </div>
  <div class="panel-body">
   
            <?php wp_nonce_field( 'manage_ace_student_' . $current_user->ID . '_' . $current_student_data->ID  ); ?>
            <input type="hidden" name="action" value="manage_ace_student" />
            <input type="hidden" name="student_id" value="<?php echo $current_student_data->ID; ?>" />
 
  <div class="form-group">
    <label for="gradelevel">Grade Level</label>

    <select name="gradelevel" class="form-control">
        <option value="">- - Select Grade Level - - </option>
    <?php foreach($levels as $level) { ?>
        <option value="<?php echo $level->term_id; ?>" <?php echo ( $current_student_gradelevel == $level->term_id ) ? 'SELECTED' : ''; ?>><?php echo $level->name; ?></option>
    <?php } ?>
    </select>

  </div>
  
 <div class="form-group">
    <label for="name">First Name</label>
    <input type="text" name="firstname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $current_student_data->user_firstname; ?>">
  </div>
  <div class="form-group">
    <label for="name">Last Name</label>
    <input type="text" name="lastname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $current_student_data->user_lastname; ?>">
  </div>
  <div class="form-group">
    <label for="name">Nick Name</label>
    <input type="text" name="nickname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $current_student_data->nickname; ?>">
  </div>
  <div class="form-group">
    <label for="name">Display Name</label>
    <input type="text" name="display_name" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $current_student_data->display_name; ?>">
  </div>
 <div class="form-group">
    <label for="email">Student Email</label>
    <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $current_student_data->user_email; ?>">
 </div>

  <div class="form-group">
    <label for="newpassword">New Password</label>
    <input type="text" name="newpassword" class="form-control" id="newpassword" placeholder="Enter New Password" value="">
  </div>
  
<div class="form-group">
    <label for="email-alerts">    <input type="checkbox" name="email-alerts" id="email-alerts" value="1" <?php echo ($current_student_emailalerts) ? 'CHECKED' : ''; ?>>  Receive Email Alerts</label>
  </div>
		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.studentform.submit();">Update</a>
        <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>" class="btn btn-danger">Cancel</a>
   </div>
   </div>
</div>
</form>

</div>
</div>
</div>

<?php break; ?>
<?php case 'activate' : ?>


<div style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
<form name="studentform" id="studentform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Activate Student : <strong><?php echo $student_data->display_name; ?></strong></h3>
  </div>
  <div class="panel-body">
   
            <?php wp_nonce_field( 'activate_ace_student_' . $current_user->ID . '_' . $_GET['uid'] ); ?>
            <input type="hidden" name="action" value="activate_ace_student" />
            <input type="hidden" name="parent_id" value="<?php echo $current_user->ID; ?>" />
            <input type="hidden" name="student_id" value="<?php echo $_GET['uid']; ?>" />
                        
<strong>Note: This will be the page confirmation for student account activation. By clicking activate now button, the user is redirected to payment gateway to process payment. Once payment is verified, user is redirected back to my students page. temporarily, the button will automatically activate student.

		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.studentform.submit();">Activate Now</a>
        <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>" class="btn btn-danger">Cancel</a>
   </div>
   </div>
</div>
</form>

</div>
</div>
</div>

<?php break; ?>
<?php case 'cancel' : ?>

<?php break; ?>
<?php case 'delete' : ?>

<div style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
<form name="studentform" id="studentform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Delete Student : <strong><?php echo $student_data->display_name; ?></strong></h3>
  </div>
  <div class="panel-body">
   
            <?php wp_nonce_field( 'delete_ace_student_' . $current_user->ID . '_' . $_GET['uid'] ); ?>
            <input type="hidden" name="action" value="delete_ace_student" />
            <input type="hidden" name="student_id" value="<?php echo $_GET['uid']; ?>" />
<strong>ALL DATA RELATED TO THIS STUDENT WILL BE REMOVED PERMANENTLY!</strong><br>
Are you sure you want to delete this student?

		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.studentform.submit();">Delete</a>
        <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>" class="btn btn-danger">Cancel</a>
   </div>
   </div>
</div>
</form>

</div>
</div>
</div>

<?php break; ?>
<?php case 'upgrade': 
$plans = AceSubscriptionPlans();
$user_plan_id = ( get_user_meta( $_GET['uid'], '_membership_plan', true) ) ? get_user_meta($_GET['uid'], '_membership_plan', true) : get_option('_ace_membership_plan_default');
$user_plan = AceSubscriptionPlans( $user_plan_id );

?>
<div style="margin-top:50px;">
<div class="row">

<?php foreach($plans as $plan) { ?>
  <div class="col-sm-6 col-md-3">
    <div class="thumbnail">
      <div class="caption">
        <h3 class="text-center"><?php echo $plan->plan_name; ?></h3>
        <p><?php echo $plan->description; ?></p>
        <?php if( $plan->plan_level > $user_plan->plan_level) { ?>
            <p class="text-center">
            <form name="studentform" id="studentform" class="login-container" action="" method="post">
            <?php wp_nonce_field( 'upgrade_ace_student_' . $current_user->ID . '_' . $_GET['uid'] ); ?>
            <input type="hidden" name="action" value="upgrade_ace_student" />
            <input type="hidden" name="student_id" value="<?php echo $_GET['uid']; ?>" />
            <input type="hidden" name="plan_id" value="<?php echo $plan->plan_id; ?>" />
                <button class="btn btn-danger btn-block" role="button" type="submit">Upgrade Now</button>
            </form>
            </p>
        <?php } else { ?>
            <p class="text-center"><a href="#upgraded" class="btn btn-success btn-block" role="button">Upgraded</a></p>
        <?php } ?>
      </div>
    </div>
  </div>
<?php } ?>
</div>
</div>

<?php break; ?>
<?php default : ?>

<header class="entry-header">
          <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=addnew" class="btn btn-success btn-sm pull-right">Add New Student</a>      
                <h1 class="entry-title">My Students</h1>
                
                 </header>


<?php 
$mystudents_raw = get_users(array( 'orderby' => 'ID', 'role' => 'student', 'meta_key' => '_parent_id', 'meta_value' => $current_user->ID) );
$mystudents = array();
$paid_count = 0;
foreach($mystudents_raw as $student_raw) {
    $paid_status = get_user_meta($student_raw->ID, '_paid', true);
    $gradelevel = get_user_meta($student_raw->ID, '_gradelevel', true);
    $mystudents[] = array(
        'data' => $student_raw,
        'paid_status' => $paid_status,
        'gradelevel' => get_term($gradelevel, 'level'),
    );
    
    if( $paid_status == 1 ) {
        $paid_count++;
    }   
    
}
$student_paid_n=0;
foreach($mystudents as $student) : 

$student_data = $student['data'];
$student_paid = $student['paid_status'];
$student_gradelevel = (! is_wp_error( $student['gradelevel'] ) ) ? $student['gradelevel']->name : '' ;

$user_plan_id = ( get_user_meta( $student['data']->ID, '_membership_plan', true) ) ? get_user_meta($student['data']->ID, '_membership_plan', true) : get_option('_ace_membership_plan_default');
$user_plan = AceSubscriptionPlans( $user_plan_id );

?>
<div class="panel panel-<?php echo ($student_paid) ? 'success' : 'danger'; ?>">
  <div class="panel-heading">
    <h3 class="panel-title">

    <span href="" class="pull-right"><small>
    <?php if( $student_paid ) { ?>
         <?php if( $paid_count > 1 ) { ?>
            <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=cancel&uid=<?php echo $student['data']->ID; ?>">Request Cancelation</a>
        <?php } ?>
    <?php 
    $student_paid_n++;
    } else { ?>
        <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=delete&uid=<?php echo $student['data']->ID; ?>">Delete</a>
    <?php } ?>
    </small></span>
    
    
    <strong><?php echo $student_data->display_name; ?></strong> <small>(<?php echo $student_data->user_login; ?> | <?php echo $student_gradelevel; ?> | <?php echo $user_plan->plan_name; ?>) </small></h3>
  </div>

<!-- <div class="panel-body">


</div>-->

<div class="panel-footer">

 <div class="btn-group btn-group-justified btn-group-sm">
 <?php if ( $student_paid ) { ?>
    <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=manage&uid=<?php echo $student['data']->ID; ?>" class="btn btn-default">Manage</a>
    <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=progress&uid=<?php echo $student['data']->ID; ?>" class="btn btn-default">Progress</a>
    <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=goals&uid=<?php echo $student['data']->ID; ?>" class="btn btn-default">Goals</a>
    <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=upgrade&uid=<?php echo $student['data']->ID; ?>" class="btn btn-default">Upgrade Plan</a>
<?php } else { ?>
     <a href="<?php echo get_permalink( get_page_by_path( 'my-students' ) ); ?>?action=activate&uid=<?php echo $student['data']->ID; ?>" class="btn btn-danger">Activate <em><strong>&quot;<?php echo $student_data->display_name; ?>&quot;</strong></em> Now</a>
<?php } ?>
</div>
</div>

</div><!-- panel -->
<?php endforeach; ?>
 

<?php break; ?>
<?php } // switch ?>

        </div>
        

      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->
  


<?php get_footer(); ?>
