<?php

$step = ( isset($_POST['step']) ) ? $_POST['step'] : '0';

$step1_error_msg = '';
    if(  isset( $GLOBALS['create_ace_account_error'] ) ) {
            $step =0;
            $step1_error = true;
            
            if(  isset( $GLOBALS['create_ace_account_error']['empty'] ) ) {
                foreach( $GLOBALS['create_ace_account_error']['empty'] as $empty ) {
                    $step1_error_msg .= '<strong>' . $empty . '</strong> should not be empty!<br>';
                }
            } 
            if(  isset( $GLOBALS['create_ace_account_error']['password'] ) ) {
                    $step1_error_msg .= '<strong>Parent Passwords</strong> don\'t match!<br>';           
            }
            if(  isset( $GLOBALS['create_ace_account_error']['userid'] ) ) {
                    $step1_error_msg .= '<strong>Parent User ID</strong> and <strong>Student User ID</strong> should not be the same!<br>';           
            }
            if(  isset( $GLOBALS['create_ace_account_error']['parent_username'] ) ) {
                    $step1_error_msg .= '<strong>Parent User ID</strong> already exists!<br>';           
            }
            if(  isset( $GLOBALS['create_ace_account_error']['parent_email'] ) ) {
                    $step1_error_msg .= '<strong>Parent Email</strong> already exists!<br>';           
            }
            if(  isset( $GLOBALS['create_ace_account_error']['student_username'] ) ) {
                    $step1_error_msg .= '<strong>Student User ID</strong> already exists!<br>';           
            }
            if(  isset( $GLOBALS['create_ace_account_error']['student_email'] ) ) {
                    $step1_error_msg .= '<strong>Student Email</strong> already exists!<br>';           
            }
    } 



get_header(); ?>

<?php
while ( have_posts() ) : the_post();
				
?>
  <div id="main-container" class="registration">
    <div class="container">

 <div class="row">
             <div class="col-md-12">
                 <header class="entry-header">
                
                <h1 class="entry-title">Registration</h1>
                
                 </header>
            </div>

        </div>
        
      <div class="row">
      

        <div class="col-md-8">
        
        
        
            <div class="main-content whitebox">
           

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<div class="entry-content">	
<form class="form-horizontal" role="form" method="post">
<?php if($step1_error) { ?>
<div class="alert alert-danger">
  <?php echo $step1_error_msg; ?>
</div>
<?php } ?>
<?php 

switch( $step ) { 
?>
<?php case '2': ?> 

    <h3>Thank you! You have successfully activated your account!</h3>
    <a class="btn btn-default" href="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>">Login <span class="glyphicon glyphicon-refresh"></span></a>
    
 <?php break; ?>
<?php case '1': ?> 
    <h3>Payment Information</h3>
    <?php wp_nonce_field( 'pay_ace_accounts_' . $GLOBALS['create_ace_account_parent'] . '_' . $GLOBALS['create_ace_account_student'] ); ?>
        <input type="hidden" name="step" value="2" />
        <input type="hidden" name="parent_id" value="<?php echo  $GLOBALS['create_ace_account_parent']; ?>" />
        <input type="hidden" name="student_id" value="<?php echo  $GLOBALS['create_ace_account_student']; ?>" />
     <input type="hidden" name="action" value="pay_ace_accounts" />
     
    <div class="alert alert-success">
      <strong>Success!</strong> You have successfully created your account!
    </div>
 
 <?php foreach( AceSubscriptionPlans() as $membership_plan ) { ?>
<div class="radio">
  <label>
    <input type="radio" name="membership_plan" id="membership_plan_<?php echo $membership_plan->plan_id; ?>" value="<?php echo $membership_plan->plan_id; ?>" <?php echo (get_option('_ace_membership_plan_default') == $membership_plan->plan_id) ? 'checked' : ''; ?>>
    <?php echo $membership_plan->plan_name; ?> - <?php echo $membership_plan->description; ?>
  </label>
</div>
<?php } ?>
    
    Note: Accounts (parent and student) are created but not yet activate. Pay now button should go to payment gateway to process payment and activate accounts but for the mean time it will directly to activate the accounts.
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
       
      <button type="submit" class="btn btn-default pull-right">Pay Now <span class="glyphicon glyphicon-chevron-right"></span></button>
       <a class="btn btn-default pull-right" href="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>">Login <span class="glyphicon glyphicon-refresh"></span></a>
    </div>
  </div>
  
   
    <?php break; ?>
<?php default: ?> 
   
    <?php wp_nonce_field( 'create_ace_account' ); ?>
    <input type="hidden" name="step" value="1" />
     <input type="hidden" name="action" value="create_ace_account" />

<h3>Parent Account Information</h3>
    
  <div class="form-group">
    <label for="parentName" class="col-sm-4 control-label">Parent Name</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="parentName" id="parentName" placeholder="Parent Name" value="<?php echo ( isset($_POST['parentName']) ) ? $_POST['parentName'] : ""; ?>">
    </div>
  </div>
  
  <div class="form-group">
    <label for="parentEmail" class="col-sm-4 control-label">Email</label>
    <div class="col-sm-5">
      <input type="email" class="form-control"  name="parentEmail" id="parentEmail" placeholder="Email" value="<?php echo ( isset($_POST['parentEmail']) ) ? $_POST['parentEmail'] : ""; ?>">
    </div>
  </div>


  <div class="form-group">
    <label for="userID" class="col-sm-4 control-label">User ID</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="userID" name="userID" placeholder="User ID" value="<?php echo ( isset($_POST['userID']) ) ? $_POST['userID'] : ""; ?>">
    </div>
  </div>
     
    <div class="form-group">
    <label for="password" class="col-sm-4 control-label">Password</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo ( isset($_POST['password']) ) ? $_POST['password'] : ""; ?>">
    </div>
  </div>
     
     
   <div class="form-group">
    <label for="confirmPassword" class="col-sm-4 control-label">Confirm Password</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" value="<?php echo ( isset($_POST['confirmPassword']) ) ? $_POST['confirmPassword'] : ""; ?>">
    </div>
  </div>
 
 <h3>Student Account Information</h3>
 
  <div class="form-group">
    <label for="studentName" class="col-sm-4 control-label">Student Name</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="studentName" name="studentName" placeholder="Student Name" value="<?php echo ( isset($_POST['studentName']) ) ? $_POST['studentName'] : ""; ?>">
    </div>
  </div>
  
    <div class="form-group">
    <label for="studentUserID" class="col-sm-4 control-label">Student User ID</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="studentUserID" name="studentUserID"  placeholder="Student User ID" value="<?php echo ( isset($_POST['studentUserID']) ) ? $_POST['studentUserID'] : ""; ?>">
    </div>
  </div>
  
    <div class="form-group">
    <label for="studentPassword" class="col-sm-4 control-label">Student Password</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="studentPassword" name="studentPassword" placeholder="Student Password" value="<?php echo ( isset($_POST['studentPassword']) ) ? $_POST['studentPassword'] : ""; ?>">
    </div>
  </div>
  
      <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default pull-right">Next <span class="glyphicon glyphicon-chevron-right"></span></button>
    </div>
  </div>
    <?php break; ?>

<?php } ?>  
</form>
		
	</div><!-- .entry-content -->
</article><!-- #post-## -->


			</div>
        </div>
        <div class="col-md-4">
            <div class="sidebar whitebox">
            <?php get_sidebar('testimonials'); ?>
           </div>
        </div>
      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->
<?php endwhile;	?>    


<?php get_footer(); ?>
