<?php 
    // only logged in users
    if ( ! is_user_logged_in() ) {
        header('location: ' . home_url() );
        exit;
    }
    
    // must have user id on query string
    if ( ! isset( $_GET['uid']) || $_GET['uid'] == '' ) {
        header('location: ' . get_permalink( get_page_by_path( 'my-students' ) ) );
        exit;
    }
    
    global $current_user;
    get_currentuserinfo();
    $parent_id = get_user_meta($_GET['uid'], '_parent_id', true);
    
    // you can't renew students not on your list
    if ( $parent_id !=  $current_user->ID ) {
        header('location: ' . get_permalink( get_page_by_path( 'my-students' ) ) );
        exit;
    }
    
    $student_data = get_userdata( $_GET['uid'] );
    
?>

<?php get_header(); ?>

<div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
   <form name="renewform" id="renewform" class="login-container" action="<?php echo get_permalink( get_page_by_path( 'renew' ) ); ?>" method="post">
       
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Renew <?php echo $student_data->data->display_name; ?></h3>
  </div>
  <div class="panel-body">
   
            <?php wp_nonce_field( 'renew_ace_account_' . $current_user->ID . '_' . $_GET['uid'] ); ?>
            <input type="hidden" name="action" value="renew_ace_account" />
            <input type="hidden" name="parent_id" value="<?php echo $current_user->ID; ?>" />
            <input type="hidden" name="student_id" value="<?php echo $_GET['uid']; ?>" />
		    
  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Pay Now</button></div>
</div>
</form>

</div>
</div>
</div>
<?php get_footer(); ?>
