<?php 
$plans = AceSubscriptionPlans();
$user_plan_id = ( get_user_meta( $current_user->ID, '_membership_plan', true) ) ? get_user_meta( $current_user->ID, '_membership_plan', true) : get_option('_ace_membership_plan_default');
$user_plan = AceSubscriptionPlans( $user_plan_id );

get_header(); ?>

<div class="container" style="margin-top:50px;">
<div class="row">

<?php foreach($plans as $plan) { ?>
  <div class="col-sm-6 col-md-3">
    <div class="thumbnail">
      <div class="caption">
        <h3 class="text-center"><?php echo $plan->plan_name; ?></h3>
        <p><?php echo $plan->description; ?></p>
        <?php if( $plan->plan_level > $user_plan->plan_level) { ?>
            <p class="text-center">
                <a href="<?php the_permalink(); ?>" class="btn btn-danger" role="button">Upgrade Now</a>
            </p>
        <?php } else { ?>
            <p class="text-center"><a href="#upgraded" class="btn btn-success" role="button">Upgraded</a></p>
        <?php } ?>
      </div>
    </div>
  </div>
<?php } ?>
</div>
</div>

<?php get_footer(); ?>
