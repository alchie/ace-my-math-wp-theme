<?php 
get_header(); ?>

<?php
while ( have_posts() ) : the_post();
				
?>
  <div id="main-container" class="">
    <div class="container">


        <div class="row">
             <div class="col-md-12">
    <?php
		// Page thumbnail and title.
		the_title( '<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->' );
	?>
            </div>
        </div>
        
      <div class="row">
      

	
        <div class="col-md-8">
            <div class="main-content whitebox">
           



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<div class="entry-content">
		<?php
			the_content();

			edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->


			</div>
        </div>
        <div class="col-md-4">
            <div class="sidebar whitebox">
            <?php get_sidebar('testimonials'); ?>
           </div>
        </div>
      </div><!-- row -->

    </div><!-- container-->
   </div><!-- #main-container -->
<?php endwhile;	?>    


<?php get_footer(); ?>
