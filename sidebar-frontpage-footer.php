 <div id="footer-widgets-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                
                <div id="secondary">
	<?php if ( is_active_sidebar( 'frontpage-footer' ) ) : ?>
	<div id="frontpage-footer" class="frontpage-footer widget-area" role="complementary">
		<?php dynamic_sidebar( 'frontpage-footer' ); ?>
	</div><!-- #primary-sidebar -->
	<?php endif; ?>
</div><!-- #secondary -->

                </div>
                
                                      
            </div>
        </div>
    </div>
