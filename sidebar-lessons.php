<?php 
$levels_raw = wp_get_post_terms( get_the_ID(), 'level' ); 
$chapters_raw = wp_get_post_terms( get_the_ID(), 'chapter' );

if( isset($levels_raw[0]) ) {
    $current_level = $levels_raw[0];
}
if( isset($chapters_raw[0]) ) {
    $current_chapter = $chapters_raw[0];
}
//if( isset( $levels ) && ( count($levels) > 0 ) ) :
//foreach($levels as $level) :

if( isset( $current_level ) && isset( $current_chapter ) ) :

   $lessons = new WP_Query(array(
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'post_type' => 'lesson',
        'posts_per_page' => -1,
        'nopaging' => true,
        'tax_query' => array(
		    array(
			    'taxonomy' => 'level',
			    'field' => 'term_id',
			    'terms' => $current_level->term_id
		    ),
		    array(
			    'taxonomy' => 'chapter',
			    'field' => 'term_id',
			    'terms' => $current_chapter->term_id
		    )
	    ),
	    'post__not_in' => array( get_the_ID() )
    )
    );
    
    if($lessons->have_posts()) :
    
?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Related Lessons</h3>
  </div>

  <div class="list-group">
<?php
     
        while( $lessons->have_posts() ) : $lessons->the_post();
        global $post;
?>
  <a href="<?php the_permalink(); ?>" class="list-group-item">Lesson <?php echo $post->menu_order; ?>: <?php the_title(); ?></a>
  <?php endwhile; ?>
  

</div>

</div>
        
           
<?php 
endif; // $lessons->have_posts()

wp_reset_query();

//endforeach; // foreach($levels as $level) :
endif; // if( isset( $levels ) && ( count($levels) > 0 ) ) :

?>
