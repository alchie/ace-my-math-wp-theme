<div id="secondary">
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<aside id="testimonials" class="block-inner widget widget_testimonials">		
		<div class="widget-header"><h3 class="widget-title">Testimonials</h3><em></em></div>
				<ul>
<?php
    $testimonials = new WP_Query(array(
        'post_type' => 'testimonial',
        'posts_per_page' => 5,
    ));
    
    if($testimonials->have_posts()) :
        while( $testimonials->have_posts() ) : $testimonials->the_post();
?>
					<li>
					    
					   <?php 
                        if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('thumbnail', array('class' => 'img-responsive img') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:150px">
                        <?php } ?>
					    <div class="testi-content">
					        <?php the_content(); ?>
					    </div>
					     <div class="clearfix"></div>
				        <div class="testi-author"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
				       
					</li>
<?php endwhile; 
  endif;
  wp_reset_query();
  ?>
				</ul>
				 
		</aside>
	</div><!-- #primary-sidebar -->
</div><!-- #secondary -->
