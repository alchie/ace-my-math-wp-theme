<?php

/*
if ( ( isset( $_POST['action'] ) && ($_POST['action'] == 'start_lesson') ) && ( ! is_user_logged_in() ) ) {
    header('location: ' . home_url('login') . '?redirect=' . urlencode( get_permalink() ) );
    exit;
} 
*/

global $current_user;
get_currentuserinfo();

$lessons_allowed = (is_user_logged_in()) ? AceCurrentUserLessonsAllowed() : AceGuestLessonsAllowed();
$lesson_restricted = ( array_search( get_the_ID(), $lessons_allowed) === FALSE );

if( $lesson_restricted ) {
        get_template_part('lesson', 'restricted');
        exit;
}
        
if ( ( isset( $_POST['lesson_id'] )  && ( $_POST['lesson_id'] != '') )
    // && ( isset( $_POST['chapter_id'] )  && ( $_POST['chapter_id'] != '') )
    // && ( isset( $_POST['level_id'] )  && ( $_POST['level_id'] != '') )
        && ( isset( $_POST['lesson_nonce'] ) &&  wp_verify_nonce( $_POST['lesson_nonce'], 'start_lesson_' . $_POST['lesson_id'] ) )
    ) {
        
        update_user_meta( $current_user->ID, '_current_level', $_POST['level_id'] );
        update_user_meta( $current_user->ID, '_current_chapter', $_POST['chapter_id'] );
        update_user_meta( $current_user->ID, '_current_lesson', $_POST['lesson_id'] );
        
        /* $user_record = aceLessonProgressRetrieve($current_user->ID, get_the_ID());
        if( count( $user_record ) > 0 ) {
            aceLessonProgressUpdate($current_user->ID, get_the_ID(), date('Y-m-d H:i:s'));
        } else {
            aceLessonProgressInsert($current_user->ID, get_the_ID(), date('Y-m-d H:i:s'));
        }
        */
        
        switch( $_POST['action'] ) {
                default:
                        get_template_part('lesson', 'preview');
                break;
                case 'video':
                        get_template_part('lesson', 'video');
                break;
                case 'exercise':
                        get_template_part('lesson', 'exercise');
                break;
                case 'worksheet':
                        get_template_part('lesson', 'worksheet');
                break;
                case 'report':
                        get_template_part('lesson', 'report');
                break;
        }

} else {
        //get_template_part('lesson', 'preview');
    header('location: ' . get_permalink( get_page_by_path( 'lessons' ) ) );
    exit;
    
}











