<?php 

$levels_allowed = AceCurrentUserLevelsAllowed();
$lessons_allowed = AceCurrentUserLessonsAllowed();

$currentuser_level = get_user_meta( $current_user->ID, '_current_level', true);
$currentuser_chapter = get_user_meta( $current_user->ID, '_current_chapter', true);
$currentuser_lesson = get_user_meta( $current_user->ID, '_current_lesson', true);

get_header(); ?>

 <div id="main-container" class="lessons-list">
    <div class="container">
      <div class="row">
<div class="col-md-1">
	
	<ul class="nav nav-pills nav-stacked nav-gradelevel">

<?php $levels_raw = get_terms('level');
$levels = array();

foreach($levels_raw as $lvlraw) {   
    $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
    if( isset( $levels[$order]) ) {
        $levels[] = $lvlraw;
    } else {
        $levels[$order] = $lvlraw;
    }
}

ksort($levels);

$current_level_slug = get_query_var( $wp_query->query_vars['taxonomy'] ); 
$current_level = get_term_by('slug', $current_level_slug, 'level' );

if( count($levels) > 0 ) :
    foreach($levels as $level) : 
    if( is_int( array_search( $level->term_id, $levels_allowed ) ) ) {
?>

 <li class="<?php if( $current_level_slug == $level->slug ) echo 'active'; ?>">
 <a href="<?php echo get_term_link( $level, 'level' ); ?>"><?php echo $level->name; ?></a>
 </li>
        
<?php } 
endforeach;
endif;
?>
</ul>

	</div>
<div class="col-md-8">

 <div class="whitebox add-padding">
  
    <div class="panel-group" id="accordion">
    
<?php 

$chapters_raw = get_terms('chapter');
$chapters = array();
foreach($chapters_raw as $chptraw) {   
    $order = (int) get_custom_termmeta($chptraw->term_id, 'menu_order', true);
    if( isset( $chapters[$order] ) ) {
        $chapters[] = $chptraw;
    } else {
        $chapters[$order] = $chptraw;
    }
}
ksort($chapters);

$c = 0;
if( count($chapters) > 0 ) :
    foreach($chapters as $chapter) :
    
    /* $lessons = new WP_Query(array(
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'post_type' => 'lesson',
        'posts_per_page' => -1,
        'tax_query' => array(
		    array(
			    'taxonomy' => 'level',
			    'field' => 'term_id',
			    'terms' => $current_level->term_id
		    ),
		    array(
			    'taxonomy' => 'chapter',
			    'field' => 'term_id',
			    'terms' => $chapter->term_id
		    )
	    ),
        )
    ); */
    
    $lessons = get_ace_lessons($current_level->term_id, $chapter->term_id) ;

    if( $lessons ) :
    
?>
  <div class="panel panel-<?php echo ($current_level->term_id == $currentuser_level && $chapter->term_id == $currentuser_chapter) ? 'primary' : 'default'; ?>">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#chapter<?php echo $chapter->term_id; ?>">
          <?php echo $chapter->name; ?> : <?php echo get_custom_termmeta($chapter->term_id, 'chapter_title_' . $current_level->term_id, true); ?>
        </a>
      </h4>
    </div>
    <div id="chapter<?php echo $chapter->term_id; ?>" class="panel-collapse collapse <?php echo ($c==0) ? 'in' : ''; ?>">
      <div class="panel-body">
        
<!--<div class="panel-group" id="lessons-collapse-<?php echo $chapter->term_id; ?>">-->

<div class="list-group">
<?php foreach( $lessons as $lesson ) {  
$lesson_restricted = ( array_search( $lesson->ID, $lessons_allowed) === FALSE );
$panel = ( $lesson->date_taken != NULL) ? 'success' : 'default';
$panel = ($lesson_restricted) ? 'danger' : $panel;

?>
    <a class="list-group-item lesson-item <?php echo ($lesson->ID==$currentuser_lesson) ? 'current' : ''; ?>">
    <span class="pull-right">
		<span class="glyphicon glyphicon-film btn btn-xs <?php echo ($lesson->date_taken != '') ? 'btn-danger':''; ?> btn-lesson-video" data-id="<?php echo $lesson->ID; ?>">
			<form action="<?php echo get_permalink($lesson->ID); ?>?show=video" method="post" id="lesson_video_<?php echo $lesson->ID; ?>">
			<input type="hidden" name="lesson_id" value="<?php echo $lesson->ID; ?>">
			<input type="hidden" name="chapter_id" value="<?php echo $chapter->term_id; ?>">
			<input type="hidden" name="level_id" value="<?php echo $current_level->term_id; ?>">
			<input type="hidden" name="action" value="video">
				<?php wp_nonce_field('start_lesson_' . $lesson->ID, 'lesson_nonce'); ?>
			</form>
		</span>
		<span class="glyphicon glyphicon-pencil btn btn-xs btn-lesson-exercise" data-id="<?php echo $lesson->ID; ?>">
			<form action="<?php echo get_permalink($lesson->ID); ?>?show=exercise" method="post" id="lesson_exercise_<?php echo $lesson->ID; ?>">
			<input type="hidden" name="lesson_id" value="<?php echo $lesson->ID; ?>">
			<input type="hidden" name="chapter_id" value="<?php echo $chapter->term_id; ?>">
			<input type="hidden" name="level_id" value="<?php echo $current_level->term_id; ?>">
			<input type="hidden" name="action" value="exercise">
				<?php wp_nonce_field('start_lesson_' . $lesson->ID, 'lesson_nonce'); ?>
			</form>
		</span>
		<span class="glyphicon glyphicon-list-alt btn btn-xs btn-lesson-worksheet" data-id="<?php echo $lesson->ID; ?>">
			<form action="<?php echo get_permalink($lesson->ID); ?>?show=worksheet" method="post" id="lesson_worksheet_<?php echo $lesson->ID; ?>">
			<input type="hidden" name="lesson_id" value="<?php echo $lesson->ID; ?>">
			<input type="hidden" name="chapter_id" value="<?php echo $chapter->term_id; ?>">
			<input type="hidden" name="level_id" value="<?php echo $current_level->term_id; ?>">
			<input type="hidden" name="action" value="worksheet">
				<?php wp_nonce_field('start_lesson_' . $lesson->ID, 'lesson_nonce'); ?>
			</form>
		</span>
		<span class="glyphicon glyphicon-signal btn btn-xs btn-lesson-report" data-id="<?php echo $lesson->ID; ?>">
			<form action="<?php echo get_permalink($lesson->ID); ?>?show=report" method="post" id="lesson_report_<?php echo $lesson->ID; ?>">
			<input type="hidden" name="lesson_id" value="<?php echo $lesson->ID; ?>">
			<input type="hidden" name="chapter_id" value="<?php echo $chapter->term_id; ?>">
			<input type="hidden" name="level_id" value="<?php echo $current_level->term_id; ?>">
			<input type="hidden" name="action" value="report">
				<?php wp_nonce_field('start_lesson_' . $lesson->ID, 'lesson_nonce'); ?>
			</form>
		</span>
    </span>

    <span class="glyphicon glyphicon-star<?php echo ($lesson->ID==$currentuser_lesson) ? '' : '-empty'; ?>"></span> <strong>Lesson <?php echo $lesson->menu_order; ?>: </strong><?php echo $lesson->post_title; ?> 
  
  </a>
  <!--
  <div class="panel panel-<?php echo $panel; ?>">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#lessons-collapse-<?php echo $chapter->term_id; ?>" href="#lesson-<?php echo $lesson->ID; ?>">
          Lesson <?php echo $lesson->menu_order; ?>: <?php echo $lesson->post_title; ?>
        </a><small class="pull-right"><?php echo $lesson->date_taken; ?></small>
      </h4>
    </div>
    <div id="lesson-<?php echo $lesson->ID; ?>" class="panel-collapse collapse">
      <div class="panel-body">
        <?php the_content(); ?>
        <?php if( $lesson_restricted ) { ?>
            <a href="<?php echo get_permalink( get_page_by_path( 'upgrade' ) ); ?>" class="btn btn-danger  btn-sm">Upgrade Now</a>
        <?php } else { ?>
        
        <form action="<?php echo get_permalink($lesson->ID); ?>" method="post">
		<input type="hidden" name="lesson_id" value="<?php echo $lesson->ID; ?>">
		<input type="hidden" name="action" value="start_lesson">
		    <?php wp_nonce_field('start_lesson_' . $lesson->ID, 'lesson_nonce'); ?>
		    <button type="submit" class="btn btn-success btn-sm">Start Lesson <?php echo $lesson->menu_order; ?></button>
		</form>
		<?php } ?>
      </div>
    </div>
  </div>
  -->
  
<?php } ?>
  
  </div>
<!--</div> panel-group -->
        
        
      </div>
    </div>
  </div>
  
  
  
<?php 

 endif;


$c++;
    endforeach;
endif;
?>
  
  
</div><!-- panel group -->
    
    </div> <!-- whitebox -->


</div><!-- col -->

      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            <?php get_sidebar('testimonials'); ?>
            
           </div>
           
        </div>  
        

      </div><!-- row -->
    </div>
 </div>

<?php get_footer(); ?>
