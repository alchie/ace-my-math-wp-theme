<?php 
/* Template Name: Free Sample */
get_header(); ?>

<?php
while ( have_posts() ) : the_post();
				
?>

 <div id="main-container" class="free-sample">
    <div class="container">
        <div class="row">
             <div class="col-md-12">
    <?php
		// Page thumbnail and title.
		the_title( '<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->' );
	?>
            </div>
        </div>
        
   
 
      <div class="row">
        <div class="col-md-8">
            <div class="main-content whitebox">
<div class="entry-content">
<?php $levels = get_terms('level'); 

foreach($levels as $lev) :
    
query_posts( array(
    'post_type' => 'lesson',
    'posts_per_page' => 5,
    'tax_query' => array(
        array(
			'taxonomy' => 'level',
			'field' => 'term_id',
			'terms' =>  $lev->term_id
		),
    )
    )
);
    
if ( have_posts() ) :
?>
<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title"><?php echo $lev->name; ?></h3></div>
<ul class="list-group">
<?php while ( have_posts() ) : the_post(); ?>
    <li class="list-group-item">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
            <div class="lesson-icons pull-right">
                <?php $video = get_post_meta(get_the_ID(), 'lesson_video_url', true); 
                if($video) { ?>
                <a href="<?php echo $video; ?>" class="fancybox fancybox.iframe"><span class="glyphicon glyphicon-facetime-video"></span></a> 
                <?php } ?>
                <a href=""><span class="glyphicon glyphicon-print"></span></a> 
                <a href=""><span class="glyphicon glyphicon-list-alt"></span></a> 
            </div>
            <div class="lesson-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
        </article><!-- #post-## -->      
    </li>
    <?php endwhile; ?>
</ul>
<div class="panel-footer">
    <a href="#" class="btn btn-success btn-xs btn-rounded pull-right">Show More</a>
    <span class="clearfix"></span>
</div>
</div>
<?php 

endif; 

wp_reset_query(); 

?>
<?php endforeach; ?>

</div>
			</div>
        </div>
        <div class="col-md-4">
            <div class="sidebar whitebox">
                <?php get_sidebar('samplepage'); ?>
           </div>
        </div>
      </div><!-- row -->
      

    </div><!-- container-->
   </div><!-- #main-container -->
<?php endwhile;	?>    


<?php get_footer(); ?>
