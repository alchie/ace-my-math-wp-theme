<?php
	/* Template Name: Worksheet Print */
?>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/print.css" type="text/css" media="screen, print" />
<style type="text/css" media="screen" >
	body {
		font-family: Helvetica, sans-serif;
	}
	
	.page {
		margin: auto;
		height: auto;
	}
	
	.body, .header {
		height: auto;
	}
	
	.footer {
		display: none;
	}
	
	.page-breaker {
		display: block;
	}
	
	.page:not(:first-child) .header {
		display: none;
	}
	
	.answer-title {
		display: block;
		background: #fff;
		width: 660px;
		margin: auto;
		padding-bottom: 20px;
		padding-left: 10px;
	}
</style>

<script>
jQuery(document).ready(function($) {
	
	function adjustOffset () {
		$('#offset').css('height', '20px');
	}

	function display() {
		var problems = window.opener.problems;
		var showAnswers = window.opener.showAnswers;
		var answers = window.opener.answers;
		var answerTypes = window.opener.answerTypes;
		
		var page;
		var header;
		var body;
		var footer;
		var row;
		var rowDivider;
		var cell;
		var listLength = problems.length;
		for (var i = 0; i < listLength; i++) {
			
			if ((i % 4) == 0) {
				page = $('<div class="page"></div>');
				header = $('<div class="header"></div>');
				header.append('<img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo2.jpg"/>');
				header.append('<h2 class="title">Problems</h2>');
				body = $('<div class="body"></div>');
				footer = $('<div class="footer"></div>');
				footer.append('<img class="go-finger" src="<?php bloginfo('template_url'); ?>/images/hand.png"/>Go to <a href="<?php echo get_site_url(); ?>">www.AceMyMath.com</a> for more worksheets, lesson videos, and online exercises');
			}
			
			if ((i % 2) == 0) {
				row = $('<div></div>');
				cell = $('<div class="col-left cell-large"></div>');
				rowDivider = $('<div class="col-left row-divider"></div>');
			}
			else {
				cell = $('<div class="col-right cell-large"></div>');
				rowDivider = $('<div class="col-right row-divider"></div>');
			}

			cell.append('<div>' + '<span class="text-bold">' + (i + 1) + '. &nbsp;</span> ' + problems[i] + '</div>');
			cell.append('<br>');
			for (var j = 0; j < answerTypes[i].length; j++) {
				cell.append('<div class="sel-option">' + answerTypes[i][j] + '</div>');
			}

			row.append(cell);
			
			if ((i % 4) == 0) {
				page.append(header);
				page.append(body);
				page.append(footer);
				$('body').append(page);	
			}
			
			if ((i % 2) == 0) {
				body.append(row);
				if ((i + 2) < listLength) {
					body.append('<div class="divider"><div class="col-left row-divider"></div><div class="col-right row-divider"></div></div>');
				}
			}
		}
		
		if (showAnswers) {
			$('body').append('<h2 class="answer-title">Answers</h2>');

			for (var i = 0; i < listLength; i++) {
			
				if ((i % 32) == 0) {
					page = $('<div class="page"></div>');
					header = $('<div class="header"></div>');
					header.append('<img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo2.jpg"/>');
					header.append('<h2 class="title">Answers</h2>');
					body = $('<div class="body"></div>');
					footer = $('<div class="footer"></div>');
					footer.append('Go to <a href="<?php echo get_site_url(); ?>">www.AceMyMath.com</a> for more worksheets, lesson videos, and online exercises');
				}
				
				if ((i % 2) == 0) {
					row = $('<div></div>');
					cell = $('<div class="col-left cell-small"></div>');
				}
				else {
					cell = $('<div class="col-right cell-small"></div>');
				}
	
				cell.append('<div>' + '<span class="text-bold">' + (i + 1) + '. &nbsp;</span> ' + answers[i] + '</div>');

				row.append(cell);
				
				if ((i % 32) == 0) {
					page.append(header);
					page.append(body);
					page.append(footer);
					$('body').append(page);	
				}
				
				if ((i % 2) == 0) {
					body.append(row);
				}
			}
		}
		
		$('.divider:odd').addClass("page-breaker");
		
		$('.input-answers').removeAttr('style');
	}

	display();
});
</script>